
$(document).ready(function() {

  console.log('loaded');

  var appData = {
    companyName: 'NovelCompany',
    content: {


    }

  }


  /**
   * Submenu
   */
   new Vue({
     el: '#submenu',
     data: {
       links: [
         {
           name: 'Get Inspired',
           root: ['Link 1', 'Link 2', 'Link 3', 'Link 4', 'Link 5']
         },
         {
           name: 'New Beginnings',
           root: ['Link 1', 'Link 2', 'Link 3']
         },
         {
           name: 'Focus',
           root: ['Link 1', 'Link 2', 'Link 3']
         },
         {
           name: 'Foster Partnerships',
           root: ['Link 1', 'Link 2', 'Link 3']
         }
       ],
       test: 'great'
     }
   });

  /**
   * Marketing
   */
   new Vue({
     el: '#marketing-content',
     data: {
       info: [
         { heading: 'Analyze', caption: 'We will get you started from the beginning.' },
         { heading: 'Strategize',  caption: 'We will work consult with you through the entire process.' },
         { heading: 'Results', caption: 'We will provide you support for continued success.' }
       ]
     }
   });


   /**
    * Body content
    */
    new Vue({
      el: '#body-content',
      data: {
        content: [
          { title: 'Get started with ', created: 'Nov 11, 2015', body: 'content text here' },
          { created: 'Oct 12, 2015', body: 'content text here' },
          { created: 'Sep  3, 2015', body: 'content text here' },
          { created: 'Aug 14, 2015', body: 'content text here'  },
          { created: 'Jul 17, 2015', body: 'content text here' },
          { created: 'Jul 6, 2015', body: 'content text here' },
          { created: 'Jun 13, 2015', body: 'content text here' },
          { created: 'May 04, 2015', body: 'content text here' },
        ]
      }
    });


  /**
   * Footer
   */

  Vue.component('social-links', {
    props: ['links'],
    template: '#social-links-template'
  });

  Vue.component('bottom-links', {
    props: ['links'],
    template: '#bottom-links-template'
  });


  new Vue({
    el: 'footer',
    data: {
      name: appData.companyName,
      links: {
        social: [
          { name: 'facebook'},
          { name: 'twitter'},
          { name: 'google-plus'},
          { name: 'youtube'}
        ],
        company: [{name: 'About Us'}, { name: 'Partners'}, { name: 'Corporate Responsibility'}, { name: 'Careers'}, { name: 'Returns & Warranty'}]
      }
    }
  });


});
